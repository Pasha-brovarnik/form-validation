const form = document.querySelector('form');
const name = document.querySelector('#name');
const phone = document.querySelector('#phone');
const email = document.querySelector('#email');
const website = document.querySelector('#website');
const password = document.querySelector('#password');
const confirmPassword = document.querySelector('#confirm-password');
const messageContainer = document.querySelector('.message-container');
const message = document.querySelector('#message');

let isValid = false;
let passwordsMatch = false;
name.style.borderColor = 'black';
phone.style.borderColor = 'black';
email.style.borderColor = 'black';
website.style.borderColor = 'black';
password.style.borderColor = 'black';
confirmPassword.style.borderColor = 'black';

function validateForm() {
	// Using constraint API
	isValid = form.checkValidity();

	// Style main message for an error
	if (!isValid) {
		message.textContent = 'Please fill out all fields';
		message.style.color = 'red';
		messageContainer.style.borderColor = 'red';
		return;
	}
	// Check to see if passwords match
	if (password.value === confirmPassword.value) {
		passwordsMatch = true;
		password.style.borderColor = 'green';
		confirmPassword.style.borderColor = 'green';
	} else {
		passwordsMatch = false;
		message.textContent = 'Make sure passwords match';
		message.style.color = 'red';
		messageContainer.style.borderColor = 'red';
		confirmPassword.style.borderColor = 'red';
		return;
	}

	// If form is valid and passwords match
	if (isValid && passwordsMatch) {
		message.textContent = 'Successfully registered!';
		message.style.color = 'green';
		messageContainer.style.borderColor = 'green';
	}
}

function storeFormData() {
	const user = {
		name: form.name.value,
		phone: form.phone.value,
		email: form.email.value,
		website: form.website.value,
		password: form.password.value,
	};
	// Do something with the user data...
	console.log('User data', user);
}

function processFormData(e) {
	e.preventDefault();
	// Validate form
	validateForm();
	// Submit form data if valid
	if (isValid && passwordsMatch) {
		storeFormData();
	}
}

function setValidationStyle({ elm, value, pattern }) {
	const regex = RegExp(pattern);

	if (!value) {
		elm.style.borderColor = 'black';
	} else if (!regex.test(value)) {
		elm.style.borderColor = 'red';
	} else {
		elm.style.borderColor = 'green';
	}
}

const validation = {
	hello: 'testing',
	name: ({ target }) => {
		if (!target.value) {
			name.style.borderColor = 'black';
		} else if ((target.value.length > 0) & (target.value.length < 3)) {
			name.style.borderColor = 'red';
		} else {
			name.style.borderColor = 'green';
		}
	},
	phone: ({ target }) => {
		const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
		setValidationStyle({ elm: phone, value: target.value, pattern });
	},
	email: ({ target }) => {
		const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		setValidationStyle({
			elm: email,
			value: target.value.toLowerCase(),
			pattern,
		});
	},
	website: ({ target }) => {
		const pattern = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
		setValidationStyle({ elm: website, value: target.value, pattern });
	},
	password: ({ target }) => {
		const pattern = /^(?=.*[\d\W])(?=.*[a-z])(?=.*[A-Z]).{8,100}$/gi;
		setValidationStyle({ elm: password, value: target.value, pattern });
		if (!confirmPassword.value.length) {
			confirmPassword.style.borderColor = 'black';
		} else if (target.value !== confirmPassword.value) {
			confirmPassword.style.borderColor = 'red';
		} else {
			confirmPassword.style.borderColor = 'green';
		}
	},
	'confirm-password': ({ target }) => {
		if (!target.value) {
			confirmPassword.style.borderColor = 'black';
		} else if (target.value !== password.value) {
			confirmPassword.style.borderColor = 'red';
		} else {
			confirmPassword.style.borderColor = 'green';
		}
	},
};

// Event listener
form.addEventListener('submit', processFormData);
form.addEventListener('input', (e) => {
	e.preventDefault();
	validation[e.target.id](e);
});
